import React from 'react';
import TodoItem from './TodoItem';
// import todosList from '../../todos.json';
// import { v4 } from 'uuid';

const TodoList = ({todos, setTodos, handleDelete, handleCheck}) => {
  
    return (
      <section className="main">
        <ul className="todo-list">
          {todos.map((todo) => (
            <TodoItem 
            key= {todo.id} 
            title={todo.title} 
            id={todo.id}
            completed={todos.completed} 
            handleCheck= {handleCheck}
            handleDelete = {handleDelete} />
          ))}
        </ul>
      </section>
    );
  }

export default TodoList