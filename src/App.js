import React, { useState } from "react";
// import { Route, Switch } from "react-router-dom";
import todosList from './todos.json';
import TodoList from './components/Todos/TodoList'
// import TodoItem from './components/Todos/TodoItem'
import { v4 } from 'uuid';

function App ()  {


  const [todos, setTodos] = useState(todosList)
  const [value, setValue] = useState('')

  // useEffect(() => {
  //   fetch (todosList)
  //   .then (response => response.json())
  //   .then (data => setTodos(data))
  //   },[])

    const handleDelete = (todoId) => {
      const newTodos = todos.filter(
        todoItem => todoItem.id !== todoId
      )
        setTodos(newTodos)
    }

    const handleCheck = (checkId) => {
      console.log("You checked")
      const newTodos = todos.map(
        todoItem => {
          if(todoItem.id === checkId) {
            todoItem.completed = !todoItem.completed
          }
          return todoItem
        }
      )
      setTodos(newTodos)
    }

    const handleCompleteDelete = () => {
      const newTodos = todos.filter(
        todoItem => todoItem.completed !== true
      )
      setTodos(newTodos)
    }

    const handleText = (event) => {
      setValue(event.target.value)
    }

    const handleSubmit = (event) => {
      if(event.key === "Enter") {
        console.log("ENTER HAS BEEN PRESSED ERMAHGERD")
        handleAddTodo()
      }
    }


    const handleAddTodo = () => {
      const newTodo = {
        "userId": 1,
        "id": v4(),
        "title": value,
        "completed": false
      }
      const newTodos = [...todos, newTodo]
      setTodos(newTodos)
    }

    // const TodoList = ({todos, setTodos, handleDelete, handleCheck}) => {
  
    //     return (
    //       <section className="main">
    //         <ul className="todo-list">
    //           {todos.map((todo) => (
    //             <TodoItem 
    //             key= {todo.id} 
    //             title={todo.title} 
    //             id={todo.id}
    //             completed={todos.completed} 
    //             handleCheck= {handleCheck}
    //             handleDelete = {handleDelete} />
    //           ))}
    //         </ul>
    //       </section>
    //     );
    //   }
  

    // const TodoItem = (props) => {
    //   return (
    //     <li className={props.completed ? "completed" : ""}>
    //       <div className="view">
    //         <input 
    //         onChange={() => props.handleCheck(props.id)}
    //         className="toggle" 
    //         type="checkbox" 
    //         checked={props.completed} />
    //         <label>{props.title}</label>
    //         <button className="destroy" 
    //         onClick={ () => props.handleDelete(props.id)}/>
    //       </div>
    //     </li>
    //   );
    // }
  
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" 
          onChange= {handleText}
          value= {value}
          onKeyDown={(event) => handleSubmit(event)}
          placeholder="What needs to be done?" 
          autoFocus />
        </header>
        <TodoList 
        todos={todos} 
        setTodos={setTodos} 
        // handleText= {handleText}
        // handleSubmit= {handleSubmit}
        handleCheck = {handleCheck}
        handleDelete = {handleDelete} 
         />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick={() => handleCompleteDelete() } className="clear-completed">Clear completed </button>
        </footer>
      </section>
    );
  }






export default App;
