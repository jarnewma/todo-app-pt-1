import React from 'react';
import { Link } from 'react-router-dom'

function Navigation(props) {
    return (
        <ul>
            <li>
                <Link to='/'> All </Link>
            </li>
            <li>
                <Link to='/welcome'> Active </Link>
            </li>
            <li>
                <Link to='/signup'> Completed </Link>
            </li>
            <>
                <Link to='*' />
            </>
        </ul>
    )
}

export default Navigation