import React from 'react'

const TodoItem = (props) => {
    return (
      <li className={props.completed ? "completed" : ""}>
        <div className="view">
          <input 
          onChange={() => props.handleCheck(props.id)}
          className="toggle" 
          type="checkbox" 
          checked={props.completed} />
          <label>{props.title}</label>
          <button className="destroy" 
          onClick={ () => props.handleDelete(props.id)}/>
        </div>
      </li>
    );
  }


export default TodoItem